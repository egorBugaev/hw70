import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

export default class App extends React.Component {
    state = {
        digits: [9, 8, 7, 6, 5, 4, 3, 2, 1, 0, '.'],
        inputValue: '',
        block: false,
        show: '',
        arrow:'<'
    };

    buttonHandler = (digit) => {
        this.setState(prevState => {
            const val = prevState.inputValue + digit.toString();
            const val1 = prevState.show + digit.toString();
            return {...prevState, inputValue: val, show: val1};

        });
    };
    getCalc = (mark) => {
        this.setState(prevState => {
            if (!prevState.block) {
                const val = prevState.inputValue + mark;
                return {...prevState, inputValue: val, block: true , show:''};
            }
            return prevState;
        });
    };
    result = () => {
        this.setState(prevState => {
            let last = prevState.inputValue.toString().slice(-1);
            if (parseInt(last) || last === '0') {
                return {...prevState, block: false, show: eval(prevState.inputValue)}
            }
        })
    };
    clear = () => {
        this.setState({inputValue: '', block: false, show:''});
    };
    back = () => {
        this.setState({block: false, inputValue: this.state.inputValue.substring(0, this.state.inputValue.length - 1),
                show: this.state.inputValue.substring(0, this.state.inputValue.length - 1)});
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>{this.state.show}</Text>
                <View style={styles.buttons}>
                    {
                        this.state.digits.map(digit => (
                            <TouchableOpacity style={styles.button} key={digit}
                                              onPress={() => this.buttonHandler(digit)}>
                                <Text style={styles.button_text}> {digit} </Text>
                            </TouchableOpacity>
                        ))
                    }
                    <TouchableOpacity style={styles.button} onPress={() => this.getCalc('+')}><Text  style={styles.button_text}> + </Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.getCalc('-')}><Text  style={styles.button_text}> - </Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.getCalc('*')}><Text  style={styles.button_text}> * </Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.getCalc('/')}><Text  style={styles.button_text}> / </Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this.result}><Text  style={styles.button_text}> = </Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this.clear}><Text  style={styles.button_text}> C </Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this.back}><Text  style={styles.button_text}> {this.state.arrow} </Text></TouchableOpacity>


                </View>
            </View>
        );


    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 40,
        flex: 1,
        backgroundColor: '#fff',

    },
    buttons: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    button: {
        width: '33%',
        height: 80,
        backgroundColor: 'grey',
        borderColor: 'black',
        borderWidth: 1,
    },
    text: {
        padding: 10,
        height: 150,
        fontSize: 100
    },
    button_text: {
        textAlign: 'center',
        fontSize:60

    }
});
